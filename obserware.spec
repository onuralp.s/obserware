%global debug_package %{nil}
%global appdomainname org.t0xic0der.obserware

Name: obserware
Version: 0.2.5
Release: 0%{?dist}
Summary: Obserware

License: GPLv3+
Url: https://gitlab.com/t0xic0der/%{name}
Source0: https://files.pythonhosted.org/packages/5e/ba/8e83450485d2bd8a718ae28588e1170f5b10f36499da13ea4720c80cd78f/%{name}-%{version}.tar.gz

ExclusiveArch: x86_64

BuildRequires: python3-devel
BuildRequires: libappstream-glib

Requires: dnf-plugins-core

%description
An advanced system monitor utility written in Python and Qt

%prep
%autosetup

%generate_buildrequires
%pyproject_buildrequires -r

%build
%pyproject_wheel

%install
%pyproject_install
%pyproject_save_files %{name}
appstream-util validate-relax --nonet %{buildroot}%{python3_sitelib}/%{name}/appdata/%{appdomainname}.metainfo.xml
mkdir -p %{buildroot}%{_datadir}/applications %{buildroot}%{_metainfodir} %{buildroot}%{_datadir}/pixmaps
cp %{buildroot}%{python3_sitelib}/%{name}/appdata/%{appdomainname}.desktop %{buildroot}%{_datadir}/applications/%{appdomainname}.desktop
cp %{buildroot}%{python3_sitelib}/%{name}/appdata/%{appdomainname}.metainfo.xml %{buildroot}%{_metainfodir}/%{appdomainname}.metainfo.xml
cp %{buildroot}%{python3_sitelib}/%{name}/appdata/%{appdomainname}.png %{buildroot}%{_datadir}/pixmaps/%{appdomainname}.png

%files -f %{pyproject_files}
%doc README.md
%license LICENSE
%{_bindir}/%{name}
%{_metainfodir}/%{appdomainname}.metainfo.xml
%{_datadir}/applications/%{appdomainname}.desktop
%{_datadir}/pixmaps/%{appdomainname}.png

%changelog

* Thu Dec 30 2021 Akashdeep Dhar <t0xic0der@fedoraproject.org> - 0.2.5-0
- v0.2.5 - Released on December 30th, 2021
- Find the release here - https://gitlab.com/t0xic0der/obserware/-/releases/v0.2.5
- Restructured the source code into a cascading layout
- Removed tabular listing of logical partitions in the logical partitions dialog box
- Added a widget-wise listing of the logical partitions in the logical partitions dialog box
- Included storage occupancy display with the use of `QProgressBar` element for each partition
- Added permalink of the screenshots on the project documentation
- Fixed variable names of the widget subclass of the physical partitions dialog box
- Transferred UI assets like fonts, images, UI files and resources to the parent directory
- Updated the documentation with newer screenshots for the recent release
- Worked on the distribution of Obserware as a standard desktop application
- Fixed the relative location of Logical Partitions widget UI file on the project storage
- Made the application available for installation/usage on Fedora COPR and Flathub

* Sun Dec 26 2021 Akashdeep Dhar <t0xic0der@fedoraproject.org> - 0.2.4-0
- v0.2.4 - Released on December 26th, 2021
- Find the release here - https://gitlab.com/t0xic0der/obserware/-/releases/v0.2.4
- Increased project visibility by adding a featured section to README.md
- Corrected maximum dimensions of the CPU Cycles dialog box
- Removed tabular listing of physical partitions in the physical partitions dialog box
- Added a widget-wise listing of the physical partitions in the physical partitions dialog box
- Included storage occupancy display with the use of `QProgressBar` element for each partition
- Reworked main window to comply with the global system colour scheme in Qt-based desktop environments
- Reworked CPU Cycles dialog box to comply with the global system colour scheme in Qt-based desktop environments
- Reworked CPU Times dialog box to comply with the global system colour scheme in Qt-based desktop environments
- Reworked Storage Counters dialog box to comply with the global system colour scheme in Qt-based desktop environments
- Reworked Network Statistics dialog box to comply with the global system colour scheme in Qt-based desktop environments
- Reworked Process Statistics dialog box to comply with the global system colour scheme in Qt-based desktop environments
- Reworked Physical Partitions dialog box to comply with the global system colour scheme in Qt-based desktop environments
- Reworked Logical Partitions dialog box to comply with the global system colour scheme in Qt-based desktop environments
- Fixed the physical partitions dialog header to be in title-case
- Updated the documentation with newer screenshots for the recent release

* Wed Dec 15 2021 Akashdeep Dhar <t0xic0der@fedoraproject.org> - 0.2.3-0
- v0.2.3 - Released on December 15th, 2021
- Find the release here - https://gitlab.com/t0xic0der/obserware/-/releases/v0.2.3
- Redesigned CPU times dialog box to better describe the purpose
- Added widget block and widget list interfaces for CPU times
- Restructured provider module schema to be more semantic
- Made the window to be initialized on center of the screen
- Updated documentation to mark recent release of the current version
- Hard-coded font styles to make it agnostic to system font styling in UI assets
- Hard-coded font styles to make it agnostic to system font styling in interface modules
- Set uniform row heights on `QTableWidget` elements used to display network devices listing
- Set uniform row heights on `QTableWidget` elements used to display physical partitions listing
- Set uniform row heights on `QTableWidget` elements used to display logical partitions listing
- Allowed for uniform scaling in accordance with the global scaling factor

* Sat Dec 04 2021 Akashdeep Dhar <t0xic0der@fedoraproject.org> - 0.2.2-0
- v0.2.2 - Released on December 4th, 2021
- Find the release here - https://gitlab.com/t0xic0der/obserware/-/releases/v0.2.2
- Added detailed graphical view for per-core CPU usage and frequency statistics
- Nested CPU times dialog box inside of CPU cycles dialog box
- Added Gitignore file configured for Python projects
- Updated documentation to mark recent release of the current version

* Fri Dec 03 2021 Akashdeep Dhar <t0xic0der@fedoraproject.org> - 0.2.1-0
- v0.2.1 - Released on December 3rd, 2021
- Find the release here - https://gitlab.com/t0xic0der/obserware/-/releases/v0.2.1
- Initialized the project
- Added interactive quick-access bottombar for easy statistics
- Added graphical landing view for CPU, memory and swap statistics in the Performance section
- Added tabular process listing view in the Processes section
- Added CPU times tabular view in the CPU times dialog box
- Added storage counter view in the storage counter dialog box
- Fixed naming on the header for storage counters dialog box
- Fixed CPU count display information from the provider
- Added tabular partition listing view in the physical and logical partitions dialog boxes
- Added per-process statistics view in the per-process dialog box
- Had project licensed under GPL 3.0 or later
- Added network statistics view in the network statistics dialog box
- Fixed height of the storage counters dialog box
- Added static information view for software and specifications in the Information section
- Fixed retrieval of useless static data for the landing view
- Fixed time format in the per-process statistics view dialog box
- Mutated all child windows to dialog boxes
- Fixed inheritance in dialog boxes for automatically centering them
- Fixed action references to non-existent processes
- Added warning level logging for operations on process interaction
- Added contributing view for attributions and license information
- Extended project documentation with screenshots and usage information
- Fixed additional rows on table views for CPU times, logical partitions, physical partitions, network statistics and process listing
- Marked first public release for current release and updated documentation
- Fixed broken references to included fonts in the application
- Fixed addition of font resources asset and performed version bump
